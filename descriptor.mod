name="The Meow Order: Last Days of the Litterbox"
version="0.0"
tags={
	"Gameplay"
	"Map"
	"Alternative History"
	"Submod"
}
picture="thumbnail.png"
dependencies = {
	"The New Order: Last Days of Europe (Dev Copy)"
	"The New Order: Last Days of Europe"
}
supported_version="1.12.*"